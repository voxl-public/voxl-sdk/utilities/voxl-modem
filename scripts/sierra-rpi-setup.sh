#!/bin/bash

# Prerequisites:
#   sudo apt-get update
#   sudo apt-get upgrade -y
#   sudo apt-get install -y udhcpc libqmi-utils
#   create /etc/qmi-network.conf with:
#     APN=broadband

sudo ifconfig wwan0 down
sudo qmicli -d /dev/cdc-wdm0 --set-expected-data-format=raw-ip
sudo ifconfig wwan0 up
sudo qmi-network /dev/cdc-wdm0 start
sudo qmi-network /dev/cdc-wdm0 status
sudo udhcpc -q -f -i wwan0
