#!/bin/bash

# Configuration
CONFIG_FILE="config.json"
SERVER_IP=$(jq -r '.server_ip' "$CONFIG_FILE")
TEST_DURATION=$(jq -r '.test_duration' "$CONFIG_FILE")

OUTPUT_FILE="iperf_data.csv"

echo "timestamp,client_ip,megabits_per_second" > "$OUTPUT_FILE"

# Plot function with dynamic client handling
function generate_plot {
    python3 plot_data.py $OUTPUT_FILE
    exit 0
}

# Trap to handle Ctrl-C
trap generate_plot SIGINT

# Function to run iperf3 test and log results with timeout
function run_iperf_test {
    local client_ip=$1
    local username=$2
    local password=$3

    echo "Attempting to run iperf3 test from client $client_ip..."
    # Using sshpass with timeout to ensure the command does not hang
    result=$(timeout 10 sshpass -p "$password" ssh -o StrictHostKeyChecking=no \
                                                  -o ConnectTimeout=10 \
                                                  $username@$client_ip "timeout 10 iperf3 -c $SERVER_IP -t $TEST_DURATION -J")
    
    local timestamp=$(date +%s) # Current timestamp
    
    # Check if the command was executed successfully and within the timeout
    if [ $? -eq 0 ]; then
        echo "iperf3 test executed successfully for client $client_ip."
        # Parse results using jq, make sure the output is not empty
        if echo "$result" | jq . > /dev/null 2>&1; then
            local bits_per_second=$(echo "$result" | jq '.end.sum_received.bits_per_second')
            local megabits_per_second=$(bc <<< "scale=2; $bits_per_second/1000000") # Convert to Mbps
            echo "$timestamp,$client_ip,$megabits_per_second" >> "$OUTPUT_FILE"
        else
            echo "Failed to parse JSON output from iperf3 for client $client_ip."
            echo "$timestamp,$client_ip,0" >> "$OUTPUT_FILE"  # Log 0 Mbps if JSON parsing fails
        fi
    else
        echo "Failed to run or complete iperf3 on client $client_ip due to timeout or other errors."
        echo "$timestamp,$client_ip,0" >> "$OUTPUT_FILE"  # Log 0 Mbps if the command fails or times out
    fi
}

# Main execution loop
while true; do
    # Reading client information into a bash array to avoid subshell issues
    IFS=$'\n' read -d '' -r -a clients < <(jq -c '.clients[]' "$CONFIG_FILE" && printf '\0')
    
    # Iterate through each client from the config
    for client in "${clients[@]}"; do
        client_ip=$(echo "$client" | jq -r '.ip')
        username=$(echo "$client" | jq -r '.username')
        password=$(echo "$client" | jq -r '.password')
        echo "Processing client $client_ip"  # Debug output
        run_iperf_test "$client_ip" "$username" "$password"
    done
    echo "Completed one cycle of tests, starting again..."
done

echo "All tests completed."
