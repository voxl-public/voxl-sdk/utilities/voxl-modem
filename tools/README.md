# Tools

## net-perf-logger.py
This Python script automates network performance testing using iperf3, where one machine acts as a server and multiple remote machines act as clients. It repeatedly executes network tests and logs the results into a CSV file for easy analysis. This setup is ideal for monitoring and analyzing network bandwidth and performance over time.

### Features
- **Automated Testing**: Continuously tests network performance at regular intervals.
- **Multi-client Support**: Handles multiple clients simultaneously.
- **Data Logging**: Records test results in a CSV file for each test cycle.
- **Configurable Settings**: All settings, including server and client details, are configurable through a JSON file.

### Dependencies
- **Python 3.x**: Ensure Python 3 is installed on your system.
- **paramiko**: Used for handling SSH connections to clients.
- **pandas**: Used for csv file handling.
- **matplotlib**: Used for generating plots.
- **iperf3**: The network testing tool must be installed on the server and all client machines.

To install system dependencies, run:
```
sudo apt install iperf3
```

To install Python dependencies, run:
```bash
pip3 install paramiko
pip3 install pandas
pip3 install matplotlib
```

### Configuration

#### config.json

The script uses a `config.json` file to set up the network configuration. Here’s how the configuration file is structured:

```
{
    "server_ip": "your_server_ip_here",
    "clients": [
        {
            "ip": "192.168.1.100",
            "username": "user1",
            "password": "pass1"
        },
        {
            "ip": "192.168.1.101",
            "username": "user2",
            "password": "pass2"
        }
    ]
}
```
Note: Replace your_server_ip_here and the client details with actual IP addresses and SSH credentials.

#### iperf_data.csv

The output CSV file `iperf_data.csv` will be generated in the same directory as the script. It logs timestamps and bits per second for each test conducted.

### Usage

To run the script, navigate to the directory containing the script and the config.json file, and execute:

```
python3 net_perf_logger.py
```

Ensure that the iperf3 server command is allowed through your firewall and that the clients have iperf3 installed and accessible.
