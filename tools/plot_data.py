import pandas as pd
import matplotlib.pyplot as plt
import sys

def plot_data(csv_file):
    # Load the CSV data into a DataFrame
    df = pd.read_csv(csv_file)

    # Debug: Print the DataFrame to see if it's loaded correctly
    print(df.head())  # Print the first few rows of the DataFrame
    print(df.columns)  # Print the column names to verify

    # Plotting
    fig, ax = plt.subplots()
    for label, grp in df.groupby('client_ip'):
        grp.plot(x='timestamp', y='megabits_per_second', ax=ax, label=label, marker='o', linestyle='-')
    
    plt.title('Network Performance Over Time')
    plt.xlabel('Timestamp')
    plt.ylabel('Throughput (Mbps)')
    plt.legend()
    plt.grid(True)
    plt.tight_layout()

    # Save and show the plot
    plt.savefig('network_performance_plot.png')
    plt.show()

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: python plot_data.py <path_to_csv_file>")
        sys.exit(1)  # Exit if no input file is given
    else:
        plot_data(sys.argv[1])
