import subprocess
import csv
import time
import json
import sys
import os
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from datetime import datetime

# Check for required libraries
required_modules = {
    'paramiko': 'pip install paramiko',
    'pandas': 'pip install pandas',
    'matplotlib': 'pip install matplotlib'
}

for module, install_command in required_modules.items():
    try:
        __import__(module)
    except ImportError:
        print(f"The '{module}' library is not installed. Please install it using '{install_command}' and try again.")
        sys.exit(1)

import paramiko  # Import paramiko after checking

def start_iperf_server():
    """Start the iperf3 server process."""
    print("Starting the iperf3 server...")
    command = ['iperf3', '-s']
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    print("iperf3 server started.")
    return process

def run_iperf_client(ip, username, password, server_ip):
    """Connect to a client via SSH and run iperf3 test."""
    print(f"Connecting to client {ip}...")
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        ssh.connect(ip, username=username, password=password)
        print(f"Connected to {ip}. Running iperf3 test...")
        stdin, stdout, stderr = ssh.exec_command(f'iperf3 -c {server_ip} -J')
        output = stdout.read()
        print(f"Test completed for client {ip}.")
        ssh.close()
        return output
    except Exception as e:
        print(f"Failed to connect or run iperf3 on {ip}: {str(e)}")
        return None

def parse_iperf_output(output):
    """Parse the JSON output from iperf3 and extract necessary data."""
    if output is None:
        print("No output received, logging as 0 b/s.")
        return {'timestamp': time.time(), 'bits_per_second': 0}
    
    print("Parsing iperf3 output...")
    try:
        data = json.loads(output)
        results = {
            'timestamp': data['start']['timestamp']['timesecs'],
            'bits_per_second': data['end']['sum_received']['bits_per_second'] / 1_000_000 # convert bps to Mbps
        }
        print("b/s:", data['end']['sum_received']['bits_per_second'])
        print("Data parsed successfully.")
        return results
    except Exception as e:
        print(f"Error parsing data: {str(e)}")
        return {'timestamp': time.time(), 'bits_per_second': 0}

def log_to_csv(data, client_ip, filename="iperf_data.csv"):
    """Log the iperf data into a CSV file, including the client IP."""
    print(f"Logging data to {filename}...")
    fields = ['timestamp', 'client_ip', 'megabits_per_second']
    with open(filename, mode='a', newline='') as file:
        writer = csv.DictWriter(file, fieldnames=fields)
        if file.tell() == 0:
            writer.writeheader()
        writer.writerow({'timestamp': data['timestamp'], 'client_ip': client_ip, 'megabits_per_second': data['bits_per_second']})
    print("Data logged successfully.")

def load_config(filename):
    """Load configuration data from a JSON file including server IP and client details."""
    print(f"Loading configuration from {filename}...")
    with open(filename, 'r') as file:
        config = json.load(file)
    print("Configuration loaded successfully.")
    return config

def plot_data(filename="iperf_data.csv", plot_filename="network_performance_plot.png"):
    """Generate plots from logged CSV data, plotting each client separately and save the plot."""
    print("Generating plot...")
    df = pd.read_csv(filename)
    df['timestamp'] = pd.to_datetime(df['timestamp'], unit='s')
    df.set_index('timestamp', inplace=True)
    
    plt.figure(figsize=(12, 6))
    ax = plt.gca()  # Get the current Axes instance on the current figure

    for client_ip in df['client_ip'].unique():
        client_data = df[df['client_ip'] == client_ip]
        plt.plot(client_data.index, client_data['megabits_per_second'], marker='o', linestyle='-', label=f"Client {client_ip}")
    
    # Set date format on x-axis
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))  # Hours and minutes; remove '%Y-%m-%d' if you don't want the date

    plt.title('Network Performance Over Time by Client')
    plt.xlabel('Time')
    plt.ylabel('Megabits Per Second')
    plt.legend(title='Client IP')
    plt.grid(True)
    plt.tight_layout()

    # Save the plot to a file
    plt.savefig(plot_filename, format='png', dpi=300)  # Save as PNG with high resolution
    plt.show()
    print(f"Plot saved as {plot_filename}")

def archive_existing_csv(filename="iperf_data.csv"):
    """Move existing CSV file to an archive directory with a timestamp."""
    if os.path.exists(filename):
        archive_dir = 'archive'
        if not os.path.exists(archive_dir):
            os.makedirs(archive_dir)
        timestamp = datetime.now().strftime("%Y%m%d_%H%M%S")
        new_filename = f"{archive_dir}/{timestamp}_{filename}"
        os.rename(filename, new_filename)
        print(f"Archived existing file to {new_filename}")

if __name__ == "__main__":
    archive_existing_csv()  # Check for existing CSV and archive it
    config = load_config('config.json')
    server_ip = config['server_ip']
    client_list = config['clients']
    server_process = start_iperf_server()
    try:
        while True:
            for client in client_list:
                output = run_iperf_client(client['ip'], client['username'], client['password'], server_ip)
                data = parse_iperf_output(output)
                log_to_csv(data, client['ip'])  # Log data with client IP
            print("Waiting for the next round of tests...")
    except KeyboardInterrupt:
        print("Stopping server and exiting...")
    finally:
        server_process.terminate()
        print("Server process terminated.")
        plot_data()  # Call to plot the data
