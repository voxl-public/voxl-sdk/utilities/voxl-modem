#!/bin/bash
#
# Modal AI Inc. 2022
# author: james@modalai.com


sudo rm -rf build/
sudo rm -rf build32/
sudo rm -rf build64/
sudo rm -rf pkg/control.tar.gz
sudo rm -rf pkg/data/
sudo rm -rf pkg/data.tar.gz
sudo rm -rf pkg/DEB/
sudo rm -rf pkg/IPK/
sudo rm -rf *.ipk
sudo rm -rf *.deb
sudo rm -rf .bash_history
sudo rm -rf src/modem/json-c/.libs/libjson-c.so.4
sudo rm -rf src/modem/libubox/libblobmsg_json.so
sudo rm -rf src/modem/libubox/libubox.so
sudo rm -rf src/modem/uqmi/uqmi
sudo rm -rf src/configband/configband
sudo rm -rf src/quectel-CM/quectel-CM
sudo rm -rf src/quectel-CM/quectel-mbim-proxy
sudo rm -rf src/quectel-CM/quectel-qmi-proxy