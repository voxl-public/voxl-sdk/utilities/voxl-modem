#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include "serial.h"

typedef unsigned short uint16_t;

#define BAND_LOCK_ARRAY_SIZE 134 + 2
unsigned char band_lock[BAND_LOCK_ARRAY_SIZE];

static const uint16_t crc_table[256] = {
    0x0000, 0x1189, 0x2312, 0x329b, 0x4624, 0x57ad, 0x6536, 0x74bf,
    0x8c48, 0x9dc1, 0xaf5a, 0xbed3, 0xca6c, 0xdbe5, 0xe97e, 0xf8f7,
    0x1081, 0x0108, 0x3393, 0x221a, 0x56a5, 0x472c, 0x75b7, 0x643e,
    0x9cc9, 0x8d40, 0xbfdb, 0xae52, 0xdaed, 0xcb64, 0xf9ff, 0xe876,
    0x2102, 0x308b, 0x0210, 0x1399, 0x6726, 0x76af, 0x4434, 0x55bd,
    0xad4a, 0xbcc3, 0x8e58, 0x9fd1, 0xeb6e, 0xfae7, 0xc87c, 0xd9f5,
    0x3183, 0x200a, 0x1291, 0x0318, 0x77a7, 0x662e, 0x54b5, 0x453c,
    0xbdcb, 0xac42, 0x9ed9, 0x8f50, 0xfbef, 0xea66, 0xd8fd, 0xc974,
    0x4204, 0x538d, 0x6116, 0x709f, 0x0420, 0x15a9, 0x2732, 0x36bb,
    0xce4c, 0xdfc5, 0xed5e, 0xfcd7, 0x8868, 0x99e1, 0xab7a, 0xbaf3,
    0x5285, 0x430c, 0x7197, 0x601e, 0x14a1, 0x0528, 0x37b3, 0x263a,
    0xdecd, 0xcf44, 0xfddf, 0xec56, 0x98e9, 0x8960, 0xbbfb, 0xaa72,
    0x6306, 0x728f, 0x4014, 0x519d, 0x2522, 0x34ab, 0x0630, 0x17b9,
    0xef4e, 0xfec7, 0xcc5c, 0xddd5, 0xa96a, 0xb8e3, 0x8a78, 0x9bf1,
    0x7387, 0x620e, 0x5095, 0x411c, 0x35a3, 0x242a, 0x16b1, 0x0738,
    0xffcf, 0xee46, 0xdcdd, 0xcd54, 0xb9eb, 0xa862, 0x9af9, 0x8b70,
    0x8408, 0x9581, 0xa71a, 0xb693, 0xc22c, 0xd3a5, 0xe13e, 0xf0b7,
    0x0840, 0x19c9, 0x2b52, 0x3adb, 0x4e64, 0x5fed, 0x6d76, 0x7cff,
    0x9489, 0x8500, 0xb79b, 0xa612, 0xd2ad, 0xc324, 0xf1bf, 0xe036,
    0x18c1, 0x0948, 0x3bd3, 0x2a5a, 0x5ee5, 0x4f6c, 0x7df7, 0x6c7e,
    0xa50a, 0xb483, 0x8618, 0x9791, 0xe32e, 0xf2a7, 0xc03c, 0xd1b5,
    0x2942, 0x38cb, 0x0a50, 0x1bd9, 0x6f66, 0x7eef, 0x4c74, 0x5dfd,
    0xb58b, 0xa402, 0x9699, 0x8710, 0xf3af, 0xe226, 0xd0bd, 0xc134,
    0x39c3, 0x284a, 0x1ad1, 0x0b58, 0x7fe7, 0x6e6e, 0x5cf5, 0x4d7c,
    0xc60c, 0xd785, 0xe51e, 0xf497, 0x8028, 0x91a1, 0xa33a, 0xb2b3,
    0x4a44, 0x5bcd, 0x6956, 0x78df, 0x0c60, 0x1de9, 0x2f72, 0x3efb,
    0xd68d, 0xc704, 0xf59f, 0xe416, 0x90a9, 0x8120, 0xb3bb, 0xa232,
    0x5ac5, 0x4b4c, 0x79d7, 0x685e, 0x1ce1, 0x0d68, 0x3ff3, 0x2e7a,
    0xe70e, 0xf687, 0xc41c, 0xd595, 0xa12a, 0xb0a3, 0x8238, 0x93b1,
    0x6b46, 0x7acf, 0x4854, 0x59dd, 0x2d62, 0x3ceb, 0x0e70, 0x1ff9,
    0xf78f, 0xe606, 0xd49d, 0xc514, 0xb1ab, 0xa022, 0x92b9, 0x8330,
    0x7bc7, 0x6a4e, 0x58d5, 0x495c, 0x3de3, 0x2c6a, 0x1ef1, 0x0f78
};

/* Calculate the CRC for a buffer using a seed of 0xffff */
uint16_t
dm_crc16 (const char *buffer, size_t len)
{
    uint16_t crc = 0xffff;

    while (len--)
            crc = crc_table[(crc ^ *buffer++) & 0xff] ^ (crc >> 8);
    return ~crc;
}

// long int getband(char *number_string)
// {
//     int num_digits = strlen(number_string);
//     if (num_digits > 2) return -1;
//     if (((num_digits == 1) || (num_digits == 2)) &&
//         (!isdigit(number_string[0]))) return -1;
//     if ((num_digits == 2) &&
//         (!isdigit(number_string[1]))) return -1;
//     long int number = strtol(number_string, NULL, 10);
//     if ((number != 0) &&
//         (number != 2) &&
//         (number != 4) &&
//         (number != 5) &&
//         (number != 17)) return -1;
//     return number;
// }

long int checkarg(char *operation_string)
{
    return strncmp(operation_string, "write", 5);
}

void help()
{
    printf("Usage: configband <operation> <port> <configured bands>\n");
    printf("       example 1: configband write /dev/ttyUSB0 0x12345678 <0x12345678> <0x12345678>\n");
    printf("       example 2: configband read /dev/ttyUSB0\n");
}

int main(int argc, char **argv) {
    printf("configband starting\n");

    if (argc < 3) {
        fprintf(stderr, "Error: Not enough arguments\n");
        help();
        return -1;
    }

    int reading = checkarg(argv[1]);
    unsigned int band_mask_1_32 = 0;
    unsigned int band_mask_33_64 = 0;
    unsigned int band_mask_65_96 = 0;

    if (( ! reading) && (argc < 4)) {
        fprintf(stderr, "Error: Not enough arguments for a write operation\n");
        help();
        return -1;
    }
    else if (( ! reading) && (argc >= 4)) {
        band_mask_1_32 = strtoul(argv[3], NULL, 16);
        if (argc >= 5) {
            band_mask_33_64 = strtoul(argv[4], NULL, 16);
        }
        if (argc >= 6) {
            band_mask_65_96 = strtoul(argv[5], NULL, 16);
        }
    }

    unsigned char band_mask0 = (band_mask_1_32 & 0x000000ff) >> 0;
    unsigned char band_mask1 = (band_mask_1_32 & 0x0000ff00) >> 8;
    unsigned char band_mask2 = (band_mask_1_32 & 0x00ff0000) >> 16;
    unsigned char band_mask3 = (band_mask_1_32 & 0xff000000) >> 24;

    printf("Band mask 1 to 32: 0x%.8x 0x%.2x 0x%.2x 0x%.2x 0x%.2x\n", band_mask_1_32,
            band_mask0, band_mask1, band_mask2, band_mask3);

    unsigned char band_mask4 = (band_mask_33_64 & 0x000000ff) >> 0;
    unsigned char band_mask5 = (band_mask_33_64 & 0x0000ff00) >> 8;
    unsigned char band_mask6 = (band_mask_33_64 & 0x00ff0000) >> 16;
    unsigned char band_mask7 = (band_mask_33_64 & 0xff000000) >> 24;

    printf("Band mask 33 to 64: 0x%.8x 0x%.2x 0x%.2x 0x%.2x 0x%.2x\n", band_mask_33_64,
            band_mask4, band_mask5, band_mask6, band_mask7);

    unsigned char band_mask8 = (band_mask_65_96 & 0x000000ff) >> 0;
    unsigned char band_mask9 = (band_mask_65_96 & 0x0000ff00) >> 8;
    unsigned char band_mask10 = (band_mask_65_96 & 0x00ff0000) >> 16;
    unsigned char band_mask11 = (band_mask_65_96 & 0xff000000) >> 24;

    printf("Band mask 65 to 96: 0x%.8x 0x%.2x 0x%.2x 0x%.2x 0x%.2x\n", band_mask_65_96,
            band_mask8, band_mask9, band_mask10, band_mask11);

    int fd;
    int wlen;

    fd = open(argv[2], O_RDWR | O_NOCTTY | O_SYNC);
    if (fd < 0) {
        printf("Error opening %s: %s\n", argv[2], strerror(errno));
        return -1;
    }
    else {
        printf("Opened serial port\n");
    }

    /* baudrate 115200, 8 bits, no parity, 1 stop bit */
    set_interface_attribs(fd, B115200);

    // NV item READ 0x26
    // NV item WRITE 0x27
    if (reading) band_lock[0] = 0x26;
    else band_lock[0] = 0x27;

    // NV item 6828 (LTE BC Config) allows us to set the available LTE bands
    band_lock[1] = 0xac;
    band_lock[2] = 0x1a;

    if ( ! reading)
    {
        // WNC M18Q2FG-1 only supports bands 2, 4, 5, and 12
        // Sierra WP7610 supports 2, 4, 5, 12, 13, 14, 17, and 66
        // Disable band 5 by not setting the bit (0x10) in band_mask0
        band_lock[3]  = band_mask0;
        band_lock[4]  = band_mask1;
        band_lock[5]  = band_mask2;
        band_lock[6]  = band_mask3;

        band_lock[7]  = band_mask4;
        band_lock[8]  = band_mask5;
        band_lock[9]  = band_mask6;
        band_lock[10] = band_mask7;

        band_lock[11] = band_mask8;
        band_lock[12] = band_mask9;
        band_lock[13] = band_mask10;
        band_lock[14] = band_mask11;
    }

    // TODO: The Qualcomm Diag message spec requires escaping / de-escaping
    // See https://cgit.freedesktop.org/ModemManager/ModemManager/tree/libqcdm/src
    // specifically file utils.c where those routines live.
    // Since we do not yet support the escaping of the characters 0x7E or 0x7D
    // we need to error out if we see one in our data.
    for (int i = 3; i <= 10; i++) {
        if ((band_lock[i] == 0x7E) || (band_lock[i] == 0x7D)) {
            fprintf(stderr, "Band mask cannot contain 0x7E or 0x7D!\n");
            return -1;
        }
    }

    printf("Adding CRC\n");

    /* Add crc */
    uint16_t crc = dm_crc16 (band_lock, BAND_LOCK_ARRAY_SIZE - 3);
    band_lock[BAND_LOCK_ARRAY_SIZE - 3] = crc & 0xFF;
    band_lock[BAND_LOCK_ARRAY_SIZE - 2] = (crc >> 8) & 0xFF;

    /* Change last byte to 0x7e */
    band_lock[BAND_LOCK_ARRAY_SIZE - 1] = 0x7e;

    printf("Writing data\n");

    wlen = write(fd, band_lock, BAND_LOCK_ARRAY_SIZE);
    if (wlen != BAND_LOCK_ARRAY_SIZE) {
        printf("Error from serial port write: %d, %d\n", wlen, errno);
    }
    tcdrain(fd);

    //usleep(10);

    unsigned char buf[1024];
    int rdlen;

    printf("Reading data\n");

    rdlen = read(fd, buf, sizeof(buf) - 1);
    if (rdlen > 0)
    {
        unsigned char *p;
        printf("Read %d:\n", rdlen);
        for (int i = 0; i < rdlen; i++)
        {
            printf("%.2x ", buf[i]);
        }
        printf("\n");
    }
    else if (rdlen < 0)
    {
        printf("Error from read: %d: %s\n", rdlen, strerror(errno));
    }

    printf("configband ending\n");

    return 0;
}
